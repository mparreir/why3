%token <string> ID
%token LEFT_PAR
%token RIGHT_PAR
%token SEMI_COLON
%token COMMA
%token EOF

%start <Ast.value option> prog

%%

prog:
  | EOF       { None }
  | v = value { Some v }
;

value:
  | LEFT_PAR; op_name = ID ; COMMA ; token_name = ID; RIGHT_PAR ; SEMI_COLON
    { Ast.Assoc (op_name, token_name)  }
  | token_1 = ID; ID ; token_2 = ID ; SEMI_COLON
    { Ast.Assoc_token (token_1, token_2) }
;
