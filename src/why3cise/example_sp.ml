open While_ast
(* open While_parser
 * open While_lexer *)

let () =
  let lexbuf = Lexing.from_channel (open_in "test1.sp") in
  ignore (While_parser.prog While_lexer.token lexbuf)
