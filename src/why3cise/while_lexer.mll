{
  open Why3
  open While_parser

  let keywords = Hashtbl.create 11
  let () =
    List.iter
      (fun (a,b) -> Hashtbl.add keywords a b)
      [
        "skip", SKIP;
        "if", IF;
        "else", ELSE;
        "let", LET;
        "in", IN;
        "requires", REQUIRES;
        "not", NOT;
        "true", TRUE;
        "false", FALSE;
        "exists", EXISTS;
        "old", OLD;
      ]
}

let space = [' ' '\t' '\r']
let dec     = ['0'-'9']
let dec_sep = ['0'-'9' '_']
let alpha  = ['a'-'z' 'A'-'Z']
let lident = ['a'-'z' '_'] alpha*

rule token = parse
  | '\n'
      { Lexing.new_line lexbuf; token lexbuf }
  | dec dec_sep* as s
      { CONST Number.(to_small_integer (int_literal ILitDec ~neg:false (Lexlib.remove_underscores s))) }
  | space+
      { token lexbuf }
  | lident as id
      { try Hashtbl.find keywords id with Not_found -> ID id }
  | ","
      { COMMA }
  | "("
      { LEFTPAR }
  | ")"
      { RIGHTPAR }
  | "{"
      { LEFTBRC }
  | "}"
      { RIGHTBRC }
  | ";"
      { SEMI_COLON }
  | "&&"
      { LOGIC_AND }
  | "||"
      { LOGIC_OR }
  | "<"
      { LT }
  | ">"
      { GT }
  | "<>"
      { LTGT }
  | "="
      { EQUALS }
  | "-"
      { MINUS }
  | "+"
      { PLUS }
  | "<="
      { L_EQ }
  | ">="
      { G_EQ }
  | eof
      { EOF }
  | "<-"
      { ASSIGN }
  | "."
      { DOT }
  | "/"
      { DIV }
  | "*"
      { TIMES }
  | "["
      { LEFTSQ }
  | "]"
      { RIGHTSQ }
  | _ as c
      { Format.eprintf "invalid: %c@." c; Lexlib.illegal_character c lexbuf }

      
