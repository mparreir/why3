open Format
open Why3
open Wstdlib
(* open Expr *)

let usage_msg = sprintf
  "Usage: %s [options] <file>.mlw" (Filename.basename Sys.argv.(0))

let opt_file   = ref None
let opt_parser = ref None
let opt_tokens = ref None

let option_list = [
  "-F", Arg.String (fun s -> opt_parser := Some s),
    "<format> select input format (default: \"why\")";
  "--format", Arg.String (fun s -> opt_parser := Some s),
    " same as -F";
  "-tokens", Arg.String (fun s -> opt_tokens := Some s),
    " tokens description file"; ]

let add_opt_file fname = match !opt_file with
  | None -> opt_file := Some fname
  | _ -> Whyconf.Args.exit_with_usage option_list usage_msg

let config, _, env =
  Whyconf.Args.initialize option_list add_opt_file usage_msg

let fname = match !opt_file with
  | None -> Whyconf.Args.exit_with_usage option_list usage_msg
  | Some f -> f

let opt_parser = !opt_parser
let opt_tokens = !opt_tokens

let mlw_file =
  let format = opt_parser in
  Env.read_file ?format Pmodule.mlw_language env fname

module Comm = struct

  open Typing
  open Ptree
  open Ity
  open Pdecl
  open Pmodule
  open Expr
  open Number

  let mk_id ?(loc=Loc.dummy_position) name =
    Ptree.{ id_str = name; id_ats = []; id_loc = loc; }

  let mk_type_decl td_loc td_ident td_def = {
    td_loc;
    td_ident;
    td_params = [];
    td_vis = Public;
    td_mut = false;
    td_inv = [];
    td_wit = [];
    td_def;
  }

  let add_type_decl () =
    let ty_id = mk_id "t" in
    let int_id = mk_id "int" in
    let ty_def = TDalias (PTtyapp (Qident int_id, [])) in
    let ty_decl = mk_type_decl ty_id.id_loc ty_id ty_def in
    let decl = Dtype [ty_decl] in
    add_decl ty_id.id_loc decl

   let mk_logic_decl ld_loc ld_ident ld_params ld_def = {
    ld_loc;
    ld_ident;
    ld_params;
    ld_type = None ;
    ld_def;
  }


  let empty_spec = {
    sp_pre     = [];
    sp_post    = [];
    sp_xpost   = [];
    sp_reads   = [];
    sp_writes  = [];
    sp_alias   = [];
    sp_variant = [];
    sp_checkrw = false;
    sp_diverge = false;
    sp_partial = false;
  }

  let mk_expr ?(expr_loc=Loc.dummy_position) expr_desc =
    { expr_desc; expr_loc }

  let mk_pattern ?(pat_loc=Loc.dummy_position) pat_desc =
    { pat_desc; pat_loc }

  let mk_term ?(term_loc=Loc.dummy_position) term_desc =
    { term_desc; term_loc }

  let ty_unit = PTtuple []
  let e_unit = Etuple []
  let unit_arg = (Loc.dummy_position, None, false, ty_unit)
  let unit_arg_b = (Loc.dummy_position, None, false, Some ty_unit)

  let use_witness () =
    let q = Qdot (Qident (mk_id "witness"), mk_id "Witness") in
    add_decl Loc.dummy_position (Duse q)


  let mk_dlet efun sf =
    let fun_id = mk_id sf in
    (fun_id, Dlet (fun_id, true, Expr.RKnone, efun))

  let mk_fun_body fun_body =
    mk_expr (Efun ([unit_arg_b], None, Ity.MaskVisible, empty_spec, fun_body))

  let mk_fun_body_arg fun_body args =
    mk_expr (Efun (args , None, Ity.MaskVisible, empty_spec, fun_body))


  let state_attr = Ident.create_attribute "state"

  let state_eq_attr = Ident.create_attribute "state_eq"

  let check_is_state id =
    Ident.Sattr.mem state_attr id.Ident.id_attrs

  let check_is_state_eq id =
    Ident.Sattr.mem state_eq_attr id.Ident.id_attrs

  let contains_state _s {mod_units} =
    let contains_state_unit = function
      | Udecl {pd_node = PDtype its_l} ->
          List.exists (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
              check_is_state id) its_l
      | _ -> false in
    List.exists contains_state_unit mod_units

  let get_fields () =
     let state_fields =
      Mstr.fold (fun k v acc ->
          if contains_state k v then
            List.fold_left (fun acc u -> match u with
                | Udecl {pd_node = PDtype its_l} when
                    List.exists
                      (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
                        check_is_state id) its_l ->
                    let state = List.find
                        (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
                           check_is_state id) its_l in
                    state.itd_fields
                |_ -> acc) [] v.mod_units
          else acc) mlw_file [] in
     state_fields

  let add_predicate fields state_id  =
    let pred_id = mk_id "eq" in
    let param1_id = mk_id "s1" in
    let param2_id = mk_id "s2" in
    let return_state = Ptree.PTtyapp (Qident (state_id) , []) in
    let arg1 = (Loc.dummy_position, Some  param1_id , false, return_state) in
    let arg2 = (Loc.dummy_position, Some  param2_id , false, return_state) in
    let s1_q = Qident param1_id in
    let s2_q = Qident param2_id in
    let s1_term = mk_term (Tident s1_q) in
    let s2_term = mk_term (Tident s2_q) in
    let rec apply_creation fields_list s1 s2 =
      match fields_list with
      | [] -> assert false
      | [field] ->
          let field_id = mk_id field in
          let field_q = Qident field_id in
          let apply1_term = mk_term (Tidapp (field_q,[s1_term])) in
          let apply2_term = mk_term (Tidapp (field_q,[s2_term])) in
          mk_term (Tinnfix (apply1_term, (mk_id (Ident.op_infix "=")),apply2_term))
      | hd :: tl ->
          let field_id = mk_id hd in
          let field_q = Qident field_id in
          let apply1_term = mk_term (Tidapp (field_q,[s1_term])) in
          let apply2_term = mk_term (Tidapp (field_q,[s2_term])) in
          let equality_term = mk_term (Tinnfix (apply1_term, (mk_id (Ident.op_infix "=")),apply2_term)) in
          mk_term (Tbinop (equality_term, Dterm.DTand , (apply_creation tl s1 s2))) in
    let apply = apply_creation fields s1_term s2_term in
    let logic_decl = mk_logic_decl Loc.dummy_position pred_id [arg1;arg2] (Some apply) in
    let dlogic = Dlogic [logic_decl] in
    add_decl pred_id.id_loc dlogic

  let add_duple_state state_id  =
    let fun_id = mk_id "dupl_state" in
    let return_state = Ptree.PTtyapp (Qident (state_id) , []) in
    let state_arg = (Loc.dummy_position, Some  (mk_id "state") , false, return_state) in
    let result_id = mk_id "res" in
    let result_pattern = mk_pattern (Pvar result_id) in
    let result_term = mk_term (Tident (Qident result_id)) in
    let state_term = mk_term (Tident (Qident (mk_id "state"))) in
    let equality_term = mk_term (Tinnfix (result_term, (mk_id (Ident.op_infix "=")), state_term)) in
    let spec = {empty_spec with sp_post = [(Loc.dummy_position , [(result_pattern , equality_term)])]} in
    let fun_body =
      Eany ([state_arg], Expr.RKnone, Some return_state, MaskVisible, spec) in
    let fun_expr = mk_expr fun_body in
    let fun_decl = Dlet (fun_id, false, Expr.RKnone, fun_expr) in
    add_decl fun_id.id_loc fun_decl

 let add_use_module ()  =
    let loc = Loc.dummy_position in
    let q = Qdot (Qident (mk_id ~loc (Filename.chop_extension fname)), mk_id ~loc "S") in
    add_decl Loc.dummy_position (Duse q)
    
  let use_module q _m =
    let mod_id = mk_id q in
    let loc = mod_id.id_loc in
    let q = Qident mod_id in
    add_decl loc (Duse q)

  let add_decl id decl =
    add_decl id.id_loc decl

  let add_fun state_name =
    let s1_id = mk_id "s1" in
    let s1_eid = mk_expr (Eident (Qident s1_id)) in
    let state_id = mk_id state_name in
    let return_state = Ptree.PTtyvar state_id in
    let state_arg = (Loc.dummy_position, Some s1_id  , false, Some return_state) in
    let balance_id = mk_id "balance" in
    let fun_body = mk_expr (Eidapp ((Qident balance_id) , [s1_eid] )) in
    let body = mk_fun_body_arg fun_body [state_arg] in
    let (id, dlet) = mk_dlet body "test_fun" in
    add_decl id dlet

  let add_mod ?(path=[]) env id =
    let mod_uc = create_module ~path env id in
    let add_decl mod_uc pd = add_pdecl ~vc:true mod_uc pd in
    let mm = Mstr.fold (fun _m {mod_units} mod_uc ->
        List.fold_left (fun mod_uc u ->Format.eprintf "hello@."; match u with
            | Udecl pd -> add_decl mod_uc pd
            | _ -> mod_uc) mod_uc mod_units) mlw_file mod_uc in
    close_module mm


  let state_eq_fun () =
    let state_eq =
      Mstr.fold (fun k v acc ->
          if contains_state k v then
            List.fold_left (fun acc u -> match u with
                | Udecl {pd_node = PDlet l_def} ->
                   begin
                     match l_def with
                     | LDsym ({rs_logic = RLls _ } as rs, exp) ->
                       let is_state_eq = check_is_state_eq rs.rs_name in
                       if is_state_eq then (rs.rs_name.id_string :: acc)
                       else acc
                     | _ -> acc
                   end
                |_ -> acc) [] v.mod_units
          else acc) mlw_file [] in
    if List.length (state_eq) = 0 then
        ""
    else
      List.hd (state_eq)

  let state_gen () =
    let state_fields = get_fields () in
    let field_values =
      Mstr.fold (fun k v acc ->
          if contains_state k v then
            List.fold_left (fun acc u -> match u with
                | Udecl {pd_node = PDtype its_l} when
                    List.exists
                      (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
                         check_is_state id) its_l ->
                    let state = List.find
                        (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
                           check_is_state id) its_l in
                    state.itd_witness
                |_ -> acc) [] v.mod_units
          else acc) mlw_file [] in
    let results = List.map2 (fun field value -> match value.e_node with
        |  Econst c  -> begin match c with
            | ConstInt ic ->
                eprintf "%s" (field.rs_name.Ident.id_string);
                let v = ic.Number.il_int in
                let s = BigInt.to_string v in
                if BigInt.lt v BigInt.zero then eprintf "(%s)" s
                else eprintf "%s" s;
                (field, c)
            | _ -> assert false  end
        | _ -> assert false ) state_fields field_values in
    results

  let get_fields_names () =
    let rsymbol_list = get_fields () in
    let name_list =
      List.map (fun a -> a.rs_name.Ident.id_string) rsymbol_list in
    name_list


  let ident_from_rs {rs_name} =
      let id_ats = Ident.Sattr.elements rs_name.Ident.id_attrs in
      let id_ats = List.map (fun a -> ATstr a) id_ats in
      let id_loc = match rs_name.Ident.id_loc with
        | None -> Loc.dummy_position | Some l -> l in
      let id_str = rs_name.Ident.id_string in
      { id_str; id_ats; id_loc; }

  let state_ident () =
    let state_records =
      Mstr.fold (fun k v acc ->
          if contains_state k v then
            List.fold_left (fun acc u -> match u with
                | Udecl {pd_node = PDtype its_l} when
                    List.exists
                      (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
                         check_is_state id) its_l ->
                    let state = List.find
                        (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
                          check_is_state id) its_l in
                    let id_str =
                      state.itd_its.its_ts.Ty.ts_name.Ident.id_string in
                    let id_ats =
                      let open Ident in
                      Sattr.elements state.itd_its.its_ts.Ty.ts_name.id_attrs in
                    let id_ats = List.map (fun a -> ATstr a) id_ats in
                    let id_loc = begin
                      match state.itd_its.its_ts.Ty.ts_name.Ident.id_loc with
                      None -> Loc.dummy_position | Some l -> l end in
                   List.cons { id_str ; id_ats; id_loc; } acc
                |_ -> acc) [] v.mod_units
          else acc) mlw_file [] in
    List.hd state_records


  let get_state_type () =
    let state_type =
      Mstr.fold (fun k v acc ->
          if contains_state k v then
            List.fold_left (fun acc u -> match u with
                | Udecl {pd_node = PDtype its_l} when
                    List.exists
                      (fun {itd_its = {Ity.its_ts = {Ty.ts_name = id}}} ->
                         check_is_state id) its_l -> List.cons ((List.nth its_l 0).itd_its.its_ts.Ty.ts_name.Ident.id_string) acc
                |_ -> acc) [] v.mod_units
          else acc) mlw_file [] in
   (List.nth state_type 0)

  let return_state witness =
    let mk_pair_qualid_const (rs, c) =
      (Qident (ident_from_rs rs), mk_expr (Ptree.Econst c)) in
    let witness = List.map mk_pair_qualid_const witness in
    mk_expr (Erecord witness)

  let mk_fun_body_spec fun_body spec  =
    mk_expr (Efun ([unit_arg_b], None, Ity.MaskVisible, spec, fun_body))

  let wrap_add_state_gen () =
    let witness = state_gen () in
    let efun = mk_fun_body (return_state witness) in
    let (id, dlet) = mk_dlet efun "state_gen" in
    add_decl id dlet

  let fun_find () =
   let name_list =  Mstr.fold (fun _k v _acc ->
        List.fold_left (fun acc u -> match u with
            | Udecl {pd_node = PDlet let_d} ->
               begin match let_d with
               | LDsym (rsymbol, cex) when cex.c_node <> Cany && (not (check_is_state_eq rsymbol.rs_name))
                 -> rsymbol.rs_name.Ident.id_string :: acc
               | _ -> acc end
            |_ -> acc) [] v.mod_units) mlw_file [] in
   List.rev name_list

  let fun_list () =
    let args_list =  Mstr.fold (fun _k v _acc ->
        List.fold_left (fun acc u -> match u with
            | Udecl {pd_node = PDlet let_d} ->
               begin match let_d with
               | LDsym (rsymbol, cex) when (cex.c_node <> Cany) && (not (check_is_state_eq rsymbol.rs_name))
                 -> rsymbol  :: acc
               | _ -> acc end
            |_ -> acc) [] v.mod_units) mlw_file [] in
    List.rev args_list

  type cases_graph =
  {
      mutable vertices : (string * Ptree.term list * Ptree.term list) list;
      mutable edges : (string * string * int) list;
      mutable root : string ;
  }

  let rec lexical_relation pre1 pre2 =
      match (pre1,pre2) with
      | (Tident id1, Tident id2) ->
         begin match (id1,id2) with
         | (Qident ident1, Qident ident2) ->
            ident1.id_str = ident2.id_str
         | _ -> false
         end
      | (Tconst c1, Tconst c2) ->
         let cmp =  Number.compare_const c1 c2 in
         if cmp = 0 then true
         else false
      | (Tif (a,b,c), Tif (d,e,f)) ->
         (lexical_relation a.term_desc d.term_desc)
         && (lexical_relation b.term_desc e.term_desc)
         && (lexical_relation c.term_desc f.term_desc)
      | (Ttrue, Ttrue) | (Tfalse, Tfalse) -> true
      | (Tnot t1, Tnot t2) ->
         lexical_relation t1.term_desc t2.term_desc
      | (Tidapp (ls1, tl1), Tidapp (ls2,tl2)) ->
         let tmp =
           List.map2 (fun a b -> lexical_relation a.term_desc b.term_desc) tl1 tl2 in
         List.for_all (fun a -> a) tmp
      | (Tbinop (a,b1,b), Tbinop (c,b2,d)) ->
         if b1 = b2 then
           (lexical_relation a.term_desc c.term_desc)
           && (lexical_relation b.term_desc d.term_desc)
         else false
      | (Tlet (id1,a,b), Tlet (id2,c,d)) ->
         if id1.id_str = id2.id_str then
           (lexical_relation a.term_desc c.term_desc)
           && (lexical_relation b.term_desc d.term_desc)
         else false
      | _ ->  false

  let rec expr_to_term expr =
    match expr with
    | Etrue -> mk_term Ttrue
    | Efalse -> mk_term Tfalse
    | Econst c -> mk_term (Tconst c)
    | Eident q -> mk_term (Tident q)
    | Easref q -> mk_term (Tasref q)
    | Eidapp (q,el) ->
       let tl = List.map (fun a -> expr_to_term a.expr_desc) el in
       mk_term (Tidapp (q,tl))
    | Eapply (e1,e2) ->
       mk_term (Tapply ((expr_to_term e1.expr_desc), (expr_to_term e2.expr_desc)))
    | Einfix (e1,id,e2) | Einnfix (e1,id,e2) ->
       mk_term (Tinfix ((expr_to_term e1.expr_desc),id,(expr_to_term e2.expr_desc)))
    | Enot e -> mk_term (Tnot (expr_to_term e.expr_desc))
    | Eif (e1,e2,e3) ->
       mk_term (Tif (expr_to_term e1.expr_desc,
                     expr_to_term e2.expr_desc,
                     expr_to_term e3.expr_desc))
    | Eand (e1,e2) ->
       mk_term (Tbinop ((expr_to_term e1.expr_desc), Dterm.DTand , (expr_to_term e2.expr_desc)))
    | Eor (e1,e2) ->
       mk_term (Tbinop ((expr_to_term e1.expr_desc), Dterm.DTor , (expr_to_term e2.expr_desc)))
    | Etuple el ->
       let tl = List.map (fun a -> expr_to_term a.expr_desc) el in
       mk_term (Ttuple tl)
    | Elet (id,ghost,kind,e1,e2) ->
       mk_term (Tlet (id,(expr_to_term e1.expr_desc),expr_to_term e2.expr_desc))
    | _ -> assert false

  let rec expr_ptree expr =
    match expr with
    | Econst c ->
       mk_expr (Ptree.Econst c)
    | Eif (e1,e2,e3) ->
       mk_expr (Ptree.Eif (expr_ptree e1.e_node,
                           expr_ptree e2.e_node,
                           expr_ptree e3.e_node))
    | Eghost e -> mk_expr (Ptree.Eghost (expr_ptree e.e_node))
    | Evar pv ->
        let p_loc = match pv.pv_vs.vs_name.id_loc with
         | None -> Loc.dummy_position | Some l -> l in
        let p_ats  = Ident.Sattr.elements pv.pv_vs.vs_name.id_attrs in
        let p_ats = List.map (fun a -> ATstr a) p_ats in
        let p_str = pv.pv_vs.vs_name.id_string in
        let p_ident = { Ptree.id_str = p_str;
                        id_ats = p_ats;
                        id_loc = p_loc;} in
        mk_expr (Ptree.Eident (Qident p_ident))
    | Elet (l_d,expr) ->
       begin match l_d with
       | LDvar (pv,e) ->
         let p_loc = match pv.pv_vs.vs_name.id_loc with
          | None -> Loc.dummy_position | Some l -> l in
         let p_ats = Ident.Sattr.elements pv.pv_vs.vs_name.id_attrs in
         let p_ats = List.map (fun a -> ATstr a) p_ats in
         let p_str = pv.pv_vs.vs_name.id_string in
         let p_ident = { Ptree.id_str = p_str;
                        id_ats = p_ats;
                        id_loc = p_loc;} in
         let e1 = expr_ptree e.e_node in
         let e2 = expr_ptree expr.e_node in
         mk_expr (Ptree.Elet (p_ident,pv.pv_ghost,Expr.RKnone,e1,e2))
       | _ -> assert false end
    | Eassign al ->
       let n_al = List.fold_left (fun acc (pv1,rs,pv2) ->
         let p1_loc = match pv1.pv_vs.vs_name.id_loc with
          | None -> Loc.dummy_position | Some l -> l in
         let p1_ats = Ident.Sattr.elements pv1.pv_vs.vs_name.id_attrs in
         let p1_ats = List.map (fun a -> ATstr a) p1_ats in
         let p1_str = pv1.pv_vs.vs_name.id_string in
         let pv1_ident = { Ptree.id_str = p1_str;
                        id_ats = p1_ats;
                        id_loc = p1_loc;} in
         let p2_loc = match pv2.pv_vs.vs_name.id_loc with
          | None -> Loc.dummy_position | Some l -> l in
         let p2_ats = Ident.Sattr.elements pv2.pv_vs.vs_name.id_attrs in
         let p2_ats = List.map (fun a -> ATstr a) p2_ats in
         let p2_str = pv2.pv_vs.vs_name.id_string in
         let pv2_ident = { Ptree.id_str = p2_str;
                        id_ats = p2_ats;
                        id_loc = p2_loc;} in
         let rs_loc = match rs.rs_name.id_loc with
          | None -> Loc.dummy_position | Some l -> l in
         let rs_ats = Ident.Sattr.elements rs.rs_name.id_attrs in
         let rs_ats = List.map (fun a -> ATstr a) rs_ats in
         let rs_str = rs.rs_name.id_string in
         let rs_ident = { Ptree.id_str = rs_str;
                        id_ats = rs_ats;
                        id_loc = rs_loc;} in
         let rs_qident = Qident rs_ident in
         let pv1_qident = Qident pv1_ident in
         let pv2_qident = Qident pv2_ident in
         let pv1_expr = mk_expr (Eident pv1_qident) in
         let pv2_expr = mk_expr (Eident pv2_qident) in
         (pv1_expr,Some rs_qident,pv2_expr) :: acc) [] al in
       mk_expr (Eassign n_al)
    | Eexec (ce,ct) ->
       begin match ce.c_node with
       | Capp (rs,pvl) ->
          let rs_loc = match rs.rs_name.id_loc with
            | None -> Loc.dummy_position | Some l -> l  in
          let rs_ats = Ident.Sattr.elements rs.rs_name.id_attrs in
          let rs_ats = List.map (fun a -> ATstr a) rs_ats in
          let rs_str = rs.rs_name.id_string in
          let rs_ident = { Ptree.id_str = rs_str;
                           id_ats = rs_ats;
                           id_loc = rs_loc;} in
          let arg_list = List.fold_left (fun acc a ->
                           let p_loc = match a.pv_vs.vs_name.id_loc with
                             | None -> Loc.dummy_position | Some l -> l in
                           let p_ats = Ident.Sattr.elements a.pv_vs.vs_name.id_attrs in
                           let p_ats = List.map (fun a -> ATstr a) p_ats in
                           let p_str = a.pv_vs.vs_name.id_string in
                           let pv_ident = { Ptree.id_str = p_str;
                                             id_ats = p_ats;
                                             id_loc = p_loc;} in
                           let pv_qident = Qident pv_ident in
                           let pv_expr = mk_expr (Eident pv_qident) in
                           pv_expr :: acc) [] pvl in
          let rs_qident = Qident rs_ident in
          mk_expr (Eidapp (rs_qident , arg_list))
       | _ -> assert false end
    | _ -> assert false

  let rec ty_pty ty =
    let open Ty in
    let open Ident in
    match (ty.Ty.ty_node) with
      | Tyvar tvs ->
        let p_loc =  match tvs.tv_name.id_loc with
         | None -> Loc.dummy_position | Some l -> l in
        let p_ats  = Ident.Sattr.elements tvs.tv_name.id_attrs in
        let p_ats = List.map (fun a -> ATstr a) p_ats in
        let p_str = tvs.tv_name.id_string in
        let p_ident = { Ptree.id_str = p_str;
                        id_ats = p_ats;
                        id_loc = p_loc;} in
        Ptree.PTtyvar p_ident
      | Tyapp (tys,ts_l) ->
        let p_loc =  match tys.ts_name.id_loc with
         | None -> Loc.dummy_position | Some l -> l in
        let p_ats  = Ident.Sattr.elements tys.ts_name.id_attrs in
        let p_ats = List.map (fun a -> ATstr a) p_ats in
        let p_str = tys.ts_name.id_string in
        let p_ident = { Ptree.id_str = p_str;
                        id_ats = p_ats;
                        id_loc = p_loc;} in
        let pty_list = List.fold_left (fun acc ty -> ty_pty ty :: acc) [] ts_l in
        Ptree.PTtyapp (Qident(p_ident), pty_list)

  let rec pattern_ptree pattern =
    match pattern with
      | Term.Pwild -> mk_pattern (Ptree.Pwild)
      | Term.Pvar vs ->
        let id_loc =  match vs.Term.vs_name.Ident.id_loc with
        | None -> Loc.dummy_position | Some l -> l in
        let id_ats  = Ident.Sattr.elements vs.Term.vs_name.Ident.id_attrs in
        let id_ats = List.map (fun a -> ATstr a) id_ats in
        let id_str = vs.Term.vs_name.Ident.id_string in
        let ident = { id_str; id_ats; id_loc; } in
        mk_pattern (Ptree.Pvar ident)
      | Term.Papp (ls,pl) ->
        let id_loc =  match ls.Term.ls_name.Ident.id_loc with
        | None -> Loc.dummy_position | Some l -> l in
        let id_ats  = Ident.Sattr.elements ls.Term.ls_name.Ident.id_attrs in
        let id_ats = List.map (fun a -> ATstr a) id_ats in
        let id_str = ls.Term.ls_name.Ident.id_string in
        let ident = { id_str; id_ats; id_loc; } in
        let pl = List.map (fun a -> pattern_ptree a.Term.pat_node) pl in
        mk_pattern (Ptree.Papp ((Qident ident), pl))
      | Term.Por (p1,p2) ->
         mk_pattern (Ptree.Por ((pattern_ptree p1.Term.pat_node) , (pattern_ptree p2.Term.pat_node)))
      | Term.Pas (p,vs) ->
        let id_loc =  match vs.Term.vs_name.Ident.id_loc with
        | None -> Loc.dummy_position | Some l -> l in
        let id_ats  = Ident.Sattr.elements vs.Term.vs_name.Ident.id_attrs in
        let id_ats = List.map (fun a -> ATstr a) id_ats in
        let id_str = vs.Term.vs_name.Ident.id_string in
        let ident = { id_str; id_ats; id_loc; } in
        mk_pattern (Ptree.Pas ((pattern_ptree p.Term.pat_node), ident ,false))

  let rec term_ptree ?oldies_list term =
    match term with
    | Term.Tvar vs ->
       let id_loc =  begin match vs.vs_name.id_loc with
          | None -> Loc.dummy_position | Some l -> l end in
       let id_ats  = Ident.Sattr.elements vs.vs_name.id_attrs in
       let id_ats = List.map (fun a -> ATstr a) id_ats in
       let id_str = ref (vs.vs_name.id_string) in
       let () =
         match oldies_list with 
         | Some l ->
            begin try 
              let tmp = Ity.restore_pv vs in
              let is_old = List.exists (fun (a,b) ->
                             Ity.pv_equal b tmp) l in
            if is_old then begin                          
              id_str := "state2" end;
            with Not_found -> () end
         | None -> ()
       in 
       let ident = { id_str = !id_str; id_ats; id_loc; } in
          mk_term (Tident (Qident ident))
    | Term.Tconst c -> mk_term (Tconst c)
    | Term.Tif (t1,t2,t3) ->
       mk_term (Ptree.Tif ((term_ptree ?oldies_list t1.Term.t_node),
                           (term_ptree ?oldies_list t2.Term.t_node),
                           (term_ptree ?oldies_list t3.Term.t_node)))
    | Term.Ttrue -> mk_term Ptree.Ttrue
    | Term.Tfalse -> mk_term Ptree.Tfalse
    | Term.Tnot t -> mk_term (Ptree.Tnot (term_ptree ?oldies_list t.Term.t_node))
    | Term.Tapp (ls, tl) ->
       let id_loc =  match ls.Term.ls_name.Ident.id_loc with
         | None -> Loc.dummy_position | Some l -> l in
       let id_ats  = Ident.Sattr.elements ls.Term.ls_name.Ident.id_attrs in
       let id_ats = List.map (fun a -> ATstr a) id_ats in
       let id_str = ls.Term.ls_name.Ident.id_string in
       let ident = { id_str; id_ats; id_loc; } in
       let ident_q = Qident ident in
       let tl = List.map (fun a -> term_ptree ?oldies_list (a.Term.t_node)) tl in
       mk_term (Ptree.Tidapp (ident_q, tl))
    | Term.Tbinop (binop,t1,t2) ->
        let open Term in
        let open Ptree in
       begin match binop with
       | Tand -> mk_term (Tbinop ((term_ptree ?oldies_list t1.t_node),
                                  Dterm.DTand ,
                                  (term_ptree ?oldies_list t2.t_node)))
       | Tor -> mk_term (Tbinop ((term_ptree ?oldies_list t1.t_node),
                                 Dterm.DTor ,
                                 (term_ptree ?oldies_list t2.t_node)))
       | Timplies -> mk_term (Tbinop ((term_ptree ?oldies_list t1.t_node),
                                      Dterm.DTimplies ,
                                      (term_ptree ?oldies_list t2.t_node)))
       | Tiff -> mk_term (Tbinop ((term_ptree ?oldies_list t1.t_node),
                                  Dterm.DTiff ,
                                  (term_ptree ?oldies_list t2.t_node)))
       end
    | Term.Tlet (t1,(vs, _b_info,t2)) ->
       let id_loc =  match vs.Term.vs_name.Ident.id_loc with
         | None -> Loc.dummy_position | Some l -> l in
       let id_ats  = Ident.Sattr.elements vs.Term.vs_name.Ident.id_attrs in
       let id_ats = List.map (fun a -> ATstr a) id_ats in
       let id_str = vs.Term.vs_name.Ident.id_string in
       let ident = { id_str; id_ats; id_loc; } in
       mk_term (Tlet (ident,
                      (term_ptree ?oldies_list t1.Term.t_node) ,
                      (term_ptree ?oldies_list t2.Term.t_node)))
    | Term.Tquant (q,(vs_l, _b_info,trigger,term)) ->
       let trigger = List.map (fun a ->
                         List.map (fun b ->
                             term_ptree ?oldies_list  b.Term.t_node) a ) trigger in
       let binder_list = List.fold_left (fun acc vs ->
                           let id_loc =  match vs.Term.vs_name.Ident.id_loc with
                           | None -> Loc.dummy_position | Some l -> l in
                           let id_ats  =
                             Ident.Sattr.elements vs.Term.vs_name.Ident.id_attrs in
                           let id_ats = List.map (fun a -> ATstr a) id_ats in
                           let id_str = vs.Term.vs_name.Ident.id_string in
                           let ident = { id_str; id_ats; id_loc;} in
                           let pty = ty_pty vs.Term.vs_ty in
                           (id_loc, (Some ident), false, Some pty) :: acc
                           ) []  vs_l in
       let open Term in
       let open Ptree in
       begin match q with
       | Tforall -> mk_term (Tquant (Dterm.DTforall,
                                     binder_list,
                                     trigger,
                                     (term_ptree ?oldies_list term.t_node)))
       | Texists -> mk_term (Tquant (Dterm.DTexists,
                                     binder_list,
                                     trigger,
                                     (term_ptree ?oldies_list term.t_node)))
       end
    | Term.Tcase (t,branch_l) ->
       let open Term in
       let p_t_list = List.fold_left (fun acc (pt, _b_info,term) ->
                        let p = pattern_ptree pt.pat_node in
                        (p, (term_ptree ?oldies_list term.t_node)) :: acc ) [] branch_l  in
       mk_term (Ptree.Tcase ((term_ptree ?oldies_list t.t_node),p_t_list))
    | Term.Teps (vs,b_info,t) ->
       term_ptree ?oldies_list t.t_node

  let rec ident_change term arg_list ident_list sufix =
    match term.term_desc with
    | Ptree.Tident qident -> begin match qident with
        | Qident id ->
            let id_with_sufix = id.id_str ^ sufix in
            if List.mem id_with_sufix arg_list then begin
              mk_term (Ptree.Tident(Qident(List.find (fun i ->
                  i.id_str = id_with_sufix) ident_list))) end
            else term
       | _ -> assert false
       end
    | Ptree.Tconst _ -> term
    | Ptree.Tif (t1,t2,t3) ->
       let t1 = ident_change t1 arg_list ident_list sufix in
       let t2 = ident_change t2 arg_list ident_list sufix in
       let t3 = ident_change t3 arg_list ident_list sufix in
       mk_term (Tif (t1,t2,t3))
    | Ptree.Ttrue -> term
    | Ptree.Tfalse -> term
    | Ptree.Tnot t ->
       let t = ident_change t arg_list ident_list sufix in
       mk_term (Tnot t)
    | Ptree.Tidapp (ls, tl) ->
        let tl =
          List.map (fun t -> ident_change t arg_list ident_list sufix) tl in
       mk_term (Tidapp (ls,tl))
    | Ptree.Tbinop (t1,binop,t2) ->
       let t1 = ident_change t1 arg_list ident_list sufix in
       let t2 = ident_change t2 arg_list ident_list sufix in
       mk_term (Tbinop (t1,binop,t2))
    | Ptree.Tlet (id,t1,t2) ->
       let t1 = ident_change t1 arg_list ident_list sufix in
       let t2 = ident_change t2 arg_list ident_list sufix in
       mk_term (Tlet (id, t1, t2))
    | Ptree.Tquant (q,bl,tll,t) ->
       let tll = List.map (fun a -> List.map (fun b -> ident_change b arg_list ident_list sufix) a) tll in
       let t = ident_change t arg_list ident_list sufix in
       mk_term (Tquant (q, bl, tll, t))
    | Ptree.Tcase (t,branch_l) ->
       let t = ident_change t arg_list ident_list sufix in
       mk_term (Tcase (t,branch_l))
    | _ -> term

  (* let rec post_change term arg_list ident_list old_list sufix =
   *   match term.term_desc with
   *   | Ptree.Tident qident -> begin match qident with
   *       | Qident id ->
   *          let id_string = id.id_str in
   *          begin
   *            try
   *            let old = List.assoc id_string old_list in
   *            mk_term (Ptree.Tident(Qident (mk_id ("state1"))))
   *            with Not_found ->             
   *              let is_old = List.exists (fun (a,b) ->
   *                               b = id_string) old_list in
   *              if is_old then
   *                mk_term (Ptree.Tident(Qident(mk_id "state1")))
   *              else let id_with_sufix = id.id_str ^ sufix in
   *                   if List.mem id_with_sufix arg_list &&
   *                        id_with_sufix <> ("state" ^ sufix) then begin
   *                       mk_term (Ptree.Tident(Qident(List.find (fun i ->
   *                          i.id_str = id_with_sufix) ident_list))) end
   *                   else term end
   *      | _ -> assert false
   *      end
   *   | Ptree.Tconst _ -> term
   *   | Ptree.Tif (t1,t2,t3) ->
   *      let t1 = post_change t1 arg_list ident_list old_list sufix in
   *      let t2 = post_change t2 arg_list ident_list old_list sufix in
   *      let t3 = post_change t3 arg_list ident_list old_list sufix in
   *      mk_term (Tif (t1,t2,t3))
   *   | Ptree.Ttrue -> term
   *   | Ptree.Tfalse -> term
   *   | Ptree.Tnot t ->
   *      let t = post_change t arg_list ident_list old_list sufix in
   *      mk_term (Tnot t)
   *   | Ptree.Tidapp (ls, tl) ->
   *       let tl =
   *         List.map (fun t -> post_change t arg_list ident_list old_list sufix) tl in
   *      mk_term (Tidapp (ls,tl))
   *   | Ptree.Tbinop (t1,binop,t2) ->
   *      let t1 = post_change t1 arg_list ident_list old_list sufix in
   *      let t2 = post_change t2 arg_list ident_list old_list sufix in
   *      mk_term (Tbinop (t1,binop,t2))
   *   | Ptree.Tlet (id,t1,t2) ->
   *      let t1 = post_change t1 arg_list ident_list old_list sufix in
   *      let t2 = post_change t2 arg_list ident_list old_list sufix in
   *      mk_term (Tlet (id, t1, t2))
   *   | Ptree.Tquant (q,bl,tll,t) ->
   *      let tll = List.map (fun a ->
   *                    List.map (fun b -> post_change b arg_list ident_list old_list sufix) a) tll in
   *      let t = post_change t arg_list ident_list old_list sufix in
   *      mk_term (Tquant (q, bl, tll, t))
   *   | Ptree.Tcase (t,branch_l) ->
   *      let t = post_change t arg_list ident_list old_list sufix in
   *      mk_term (Tcase (t,branch_l))
   *   | _ -> term   *)     

  let rec assume_term_creation list =
    match list with
    | [] -> mk_term Ttrue
    | [term]   -> term
    | hd :: tl -> mk_term (Tbinop (hd, Dterm.DTand, (assume_term_creation tl)))

  let rec arg_expr arg_l ident_l assume sufix =
      match arg_l with
      | [] -> assert false
      | [arg] ->
         let ident =
           List.find (fun a ->
               a.id_str = arg.pv_vs.Term.vs_name.Ident.id_string ^ sufix) ident_l in
         let body =
           mk_expr (Eany ([],
                          Expr.RKnone,
                          Some (ty_pty arg.pv_vs.Term.vs_ty),
                          MaskVisible, empty_spec)) in
         mk_expr (Ptree.Elet ((ident), false, Expr.RKnone,body, assume))
      | hd :: tl ->
         let ident =
           List.find (fun a -> a.id_str = hd.pv_vs.Term.vs_name.Ident.id_string ^ sufix) ident_l in
         let body =
           mk_expr (Eany ([], Expr.RKnone, Some (ty_pty hd.pv_vs.Term.vs_ty), MaskVisible, empty_spec)) in
         mk_expr (Ptree.Elet ((ident), false, Expr.RKnone, body, arg_expr tl ident_l assume sufix))

  let get_arg func arg_name =
    List.hd (List.filter (fun a ->
                 a.pv_vs.vs_name.id_string = arg_name) func.rs_cty.cty_args)

  let same_type_args arg1 arg2 =
    let arg1_type = arg1.pv_ity.ity_node in
    let arg2_type = arg2.pv_ity.ity_node in
    match arg1_type with
    | Ityvar tv ->
       let arg1_typename = tv.tv_name.id_string in
       begin match arg2_type with
       | Ityvar tv1 ->
          let arg2_typename = tv1.tv_name.id_string in
          arg1_typename = arg2_typename
       | _ -> true
       end
    | _ -> true 
                  
  let dependency_analysis fun1_name fun2_name fun_list state_eq =
    let func1 = List.find (fun a ->
                    a.rs_name.id_string = fun1_name) fun_list in
    let func2 = List.find (fun a ->
                    a.rs_name.id_string = fun2_name) fun_list in
    let pre_list1 = List.map (fun a ->
                        term_ptree a.Term.t_node) func1.rs_cty.cty_pre in
    let arg_list1 =
      List.fold_left (fun acc a ->
          a.pv_vs.Term.vs_name.Ident.id_string :: acc) [] func1.rs_cty.cty_args in
    let arg_list1 = List.map (fun a -> a ^ "1") arg_list1 in
    let pre_list2 = List.map (fun a ->
                        term_ptree a.Term.t_node) func2.rs_cty.cty_pre in
    let arg_list2 =
      List.fold_left (fun acc a ->
          a.pv_vs.Term.vs_name.Ident.id_string :: acc) [] func2.rs_cty.cty_args in
    let arg_list2 = List.map (fun a -> a ^ "2") arg_list2 in
    let state1_qid = Qident (mk_id "state1") in
    let state2_qid = Qident (mk_id "state2") in
    let state1_term = mk_term (Tident (state1_qid)) in
    let state2_term = mk_term (Tident (state2_qid)) in
    let equality_id = ref (Qident (mk_id "eq")) in
    if state_eq <> "" then equality_id := (Qident (mk_id state_eq));
    let equal_term = mk_term (Tidapp (!equality_id , [state1_term ; state2_term] )) in
    let assume_term1 = assume_term_creation pre_list1 in
    let ident_list1 = List.fold_left (
                          fun acc a -> (mk_id a) :: acc) [] arg_list1 in
    let assume_term1 =
      ident_change assume_term1 arg_list1 ident_list1 "1" in
    let assume_term =
      (mk_term (Tbinop (assume_term1, Dterm.DTand, equal_term))) in
    let assume_expr1 = mk_expr (Ptree.Eassert (Assume, assume_term)) in
    let first_call_args =
      List.map (fun a -> (mk_expr (Eident (Qident (mk_id a))))) arg_list1 in
    let first_call =
      mk_expr (Eidapp ( (Qident (mk_id fun1_name)), List.rev first_call_args)) in
    let assume_term2 = assume_term_creation pre_list2 in
    let ident_list2 = List.fold_left (
                          fun acc a -> (mk_id a) :: acc) [] arg_list2 in
    let assume_term2 =
      ident_change assume_term2 ["state1"]  ident_list1 "1" in
    let assume_term2 =
      ident_change assume_term2 arg_list2 ident_list2 "2" in
    let assume_expr2 =
      mk_expr (Ptree.Eassert (Assume, assume_term2)) in
    let second_call_args =
      List.map (fun a -> (mk_expr (Eident (Qident (mk_id a))))) arg_list2 in
    let second_call =
      mk_expr (Eidapp ( (Qident (mk_id fun2_name)), List.rev second_call_args)) in
    let last_call =
      mk_expr (Esequence (assume_expr2, second_call)) in
    let start_call =
      mk_expr (Esequence (first_call, last_call)) in
    let assume = mk_expr (Esequence (assume_expr1, start_call)) in
    let arg2_e = arg_expr func2.rs_cty.cty_args ident_list2 assume "2" in
    let arg1_e = arg_expr func1.rs_cty.cty_args ident_list1 arg2_e "1" in
    let body = mk_fun_body arg1_e in
    let (id, dlet) = mk_dlet body (fun2_name ^ ("_" ^ ((fun1_name  ^ "_dependency")))) in
    add_decl id dlet
         
  let commutativity fun_list fun1_name fun2_name state_eq arg_tokens =
    let func1 = List.find (fun a -> a.rs_name.id_string = fun1_name) fun_list in
    let func2 = List.find (fun a -> a.rs_name.id_string = fun2_name) fun_list in
    let assume_args = ref [] in
    let inequality_id = mk_id (Ident.op_infix "<>") in
    begin if List.length arg_tokens > 0 then
      List.iter (fun ((op1,arg_n1,t1),(op2,arg_n2,t2)) ->
          let arg1 = get_arg func1 arg_n1 in
          let arg2 = get_arg func2 arg_n2 in
          let same_type = same_type_args arg1 arg2 in
          if same_type then
            let arg1_id = mk_id (arg_n1 ^ "1") in
            let arg2_id = mk_id (arg_n2 ^ "2") in
            let arg1_q = Qident arg1_id in
            let arg2_q = Qident arg2_id in
            let arg1_term = mk_term (Tident arg1_q) in
            let arg2_term = mk_term (Tident arg2_q) in
            let inequality_term = mk_term (Tinfix (arg1_term , inequality_id ,arg2_term)) in
            assume_args := inequality_term :: !assume_args) arg_tokens; end;
    let args_term = assume_term_creation !assume_args in
    let pre_list1 = List.map (fun a ->
                        term_ptree a.Term.t_node) func1.rs_cty.cty_pre in
    let arg_list1 =
      List.fold_left (fun acc a ->
          a.pv_vs.Term.vs_name.Ident.id_string :: acc) [] func1.rs_cty.cty_args in
    let arg_list1 = List.map (fun a -> a ^ "1") arg_list1 in
    let pre_list2 = List.map (fun a -> term_ptree a.Term.t_node) func2.rs_cty.cty_pre in
    let arg_list2 =
      List.fold_left (fun acc a ->
          a.pv_vs.Term.vs_name.Ident.id_string :: acc) [] func2.rs_cty.cty_args in
    let arg_list2 = List.map (fun a -> a ^ "2") arg_list2 in
    let assume_term1 = assume_term_creation pre_list1 in
    let ident_list1 = List.fold_left (
                          fun acc a -> (mk_id a) :: acc) [] arg_list1 in
    let assume_term1 = ident_change assume_term1 arg_list1 ident_list1 "1" in
    let assume_term2 = assume_term_creation pre_list2 in
    let ident_list2 = List.fold_left (
                          fun acc a -> (mk_id a) :: acc) [] arg_list2 in
    let assume_term2 = ident_change assume_term2 arg_list2 ident_list2 "2" in
    let state1_qid = Qident (mk_id "state1") in
    let state2_qid = Qident (mk_id "state2") in
    let state1_term = mk_term (Tident (state1_qid)) in
    let state2_term = mk_term (Tident (state2_qid)) in
    let equality_id = ref (Qident (mk_id "eq")) in
    if state_eq <> "" then equality_id := (Qident (mk_id state_eq));
    let equal_term = mk_term (Tidapp (!equality_id , [state1_term ; state2_term] )) in
    let assume_term = ref (mk_term (Tbinop (assume_term1, Dterm.DTand, assume_term2))) in
    assume_term := mk_term (Tbinop (!assume_term, Dterm.DTand, equal_term ));
    if List.length arg_tokens > 0 then
      assume_term := mk_term (Tbinop (args_term, Dterm.DTand, !assume_term));
    let assume_expr = mk_expr (Ptree.Eassert (Assume, !assume_term)) in
    let func1_args_firstcall = List.map (fun a -> (mk_expr (Eident (Qident (mk_id a))))) arg_list1 in
    let func2_args_firstcall = List.map (fun a ->
                                   if a <> "state2" then
                                     (mk_expr (Eident (Qident (mk_id a))))
                                   else
                                     (mk_expr (Eident (Qident (mk_id "state1"))))
                              ) arg_list2 in
    let func1_args_secondcall = List.map (fun a ->
                                    if a <> "state1" then
                                     (mk_expr (Eident (Qident (mk_id a))))
                                   else
                                     (mk_expr (Eident (Qident (mk_id "state2"))))
                              ) arg_list1 in
    let func2_args_secondcall = List.map (fun a -> (mk_expr (Eident (Qident (mk_id a))))) arg_list2 in
    let fun1_firstcall = mk_expr (Eidapp ( (Qident (mk_id fun1_name)), List.rev func1_args_firstcall)) in
    let fun2_firstcall = mk_expr (Eidapp ( (Qident (mk_id fun2_name)), List.rev func2_args_firstcall)) in
    let fun1_secondcall = mk_expr (Eidapp ( (Qident (mk_id fun1_name)), List.rev func1_args_secondcall)) in
    let fun2_secondcall = mk_expr (Eidapp ( (Qident (mk_id fun2_name)), List.rev func2_args_secondcall)) in
    let tuple = mk_expr (Etuple [mk_expr (Eident state1_qid) ; mk_expr (Eident state2_qid)]) in
    let fun2_seq2 = mk_expr (Esequence (fun2_secondcall, tuple)) in
    let fun1_seq2 = mk_expr (Esequence (fun1_secondcall, fun2_seq2)) in
    let fun1_seq1 = mk_expr (Esequence (fun1_firstcall, fun1_seq2)) in
    let fun2_seq1 = mk_expr (Esequence (fun2_firstcall , fun1_seq1)) in
    let assume = mk_expr (Esequence (assume_expr, fun2_seq1)) in
    let arg2_e = arg_expr func2.rs_cty.cty_args ident_list2 assume "2" in
    let arg1_e = arg_expr func1.rs_cty.cty_args ident_list1 arg2_e "1" in
    let x1_id = mk_id "x1" in
    let x2_id = mk_id "x2" in
    let s1_pattern = mk_pattern (Pvar x1_id) in
    let s2_pattern = mk_pattern (Pvar x2_id) in
    let pattern = mk_pattern (Ptuple [s1_pattern ; s2_pattern]) in
    let x1_q = Qident x1_id in
    let x2_q = Qident x2_id in
    let x1_term = mk_term (Tident x1_q) in
    let x2_term = mk_term (Tident x2_q) in
    let equality_term = mk_term (Tidapp (!equality_id , [x1_term ; x2_term] )) in
    let spec = {empty_spec with sp_post = [(Loc.dummy_position , [(pattern , equality_term)])]} in
    let body = mk_fun_body_spec arg1_e spec  in
    let (id, dlet) = mk_dlet body (fun1_name ^ "_" ^ fun2_name ^ "_commutativity")  in
    add_decl id dlet

  let stability operation fun_list arg_tokens state_eq  =
    let assume_args = ref [] in
    let inequality_id = mk_id (Ident.op_infix "<>") in
    begin if List.length arg_tokens > 0 then
      List.iter (fun ((op1,arg1,t1),(op2,arg2,t2)) ->
          let a1 = get_arg operation arg1 in
          let a2 = get_arg operation arg2 in
          let same_type = same_type_args a1 a2 in
          if same_type then
            let arg1_id = mk_id (arg1 ^ "1") in
            let arg2_id = mk_id (arg2 ^ "2") in
            let arg1_q = Qident arg1_id in
            let arg2_q = Qident arg2_id in
            let arg1_term = mk_term (Tident arg1_q) in
            let arg2_term = mk_term (Tident arg2_q) in
            let inequality_term = mk_term (Tinfix (arg1_term , inequality_id ,arg2_term)) in
            assume_args := inequality_term :: !assume_args) arg_tokens; end;
    let equality_id = ref (Qident (mk_id "eq")) in
    if state_eq <> "" then equality_id := (Qident (mk_id state_eq));
    let state1_id = mk_id "state1" in
    let state2_id = mk_id "state2" in
    let state1_q = Qident state1_id in
    let state2_q = Qident state2_id in
    let state1_term = mk_term (Tident state1_q) in
    let state2_term = mk_term (Tident state2_q) in
    let equality_term = mk_term (Tidapp (!equality_id , [state1_term ; state2_term] )) in
    let pre_list = List.map (fun a -> term_ptree a.Term.t_node) operation.rs_cty.cty_pre in
    let args_term = assume_term_creation !assume_args in
    let arg_list =
      List.fold_left (fun acc a ->
          ((a.pv_vs.Term.vs_name.Ident.id_string) ^ "1") :: acc) [] operation.rs_cty.cty_args in
    let arg_list1 =
      List.fold_left (fun acc a ->
          ((a.pv_vs.Term.vs_name.Ident.id_string) ^ "2") :: acc) [] operation.rs_cty.cty_args in
    let ident_list = List.fold_left (
                         fun acc a -> (mk_id a) :: acc) [] arg_list in
    let ident_list1 = List.fold_left (
                         fun acc a -> (mk_id a) :: acc) [] arg_list1 in
    let assume_term1 = assume_term_creation pre_list in
    let assume_term1 = ident_change assume_term1 arg_list ident_list "1" in
    let assume_term2 = assume_term_creation pre_list in
    let assume_term2 = ident_change assume_term2 arg_list1 ident_list1 "2" in
    let assume_term = ref (mk_term (Tbinop (assume_term1, Dterm.DTand, assume_term2))) in
    let arg_list1 = List.filter (fun a -> a <> "state2") arg_list1 in
    let arg_list1 = "state1" :: arg_list1 in
    if List.length arg_tokens > 0 then
      assume_term := mk_term (Tbinop (args_term, Dterm.DTand, !assume_term));
    assume_term := mk_term (Tbinop (equality_term, Dterm.DTand, !assume_term)) ;
    let assume_expr = mk_expr (Ptree.Eassert (Assume, !assume_term)) in
    let fun_name = List.find (fun a -> a = operation.rs_name.id_string) fun_list in
    let id_arg_list = List.map (fun a -> (mk_expr (Eident (Qident (mk_id a))))) arg_list in
    let id_arg_list = List.rev id_arg_list in
    let id_arg_list1 = List.map (fun a -> (mk_expr (Eident (Qident (mk_id a))))) arg_list1 in
    let id_arg_list1 = List.rev id_arg_list1 in
    let fun_call1 =  mk_expr (Eidapp ( (Qident (mk_id fun_name)), id_arg_list)) in
    let fun_call2 = mk_expr (Eidapp ( (Qident (mk_id fun_name)), id_arg_list1)) in
    let calls = mk_expr (Esequence (fun_call1, fun_call2)) in
    let assume = mk_expr (Esequence (assume_expr, calls)) in
    let arg2_e = arg_expr operation.rs_cty.cty_args ident_list1 assume "2" in
    let arg_e = arg_expr operation.rs_cty.cty_args ident_list arg2_e "1" in
    let body = mk_fun_body arg_e in
    let (id, dlet) = mk_dlet body (fun_name  ^ "_stability") in
    add_decl id dlet

  let diff_name_pairs fun_list no_analysis =
    List.fold_left (fun acc a ->
        List.fold_left (fun acc1 a1 ->
            if (a1 <> a)
               && (not (List.mem (a1,a) acc))
               && (not (List.mem (a,a1) acc))
               && (not (List.mem (a,a1) no_analysis))
               && (not (List.mem (a1,a) no_analysis)) then
              List.cons (a,a1) acc1
            else acc1) acc fun_list) [] fun_list  

  let no_analysis fun_list op_tokens conf_relation =
    let aux = List.filter (fun (a,b) -> a <> b) conf_relation in
    List.fold_left (fun acc (a,b) ->
        let option1 = List.find_opt (fun (c,d) -> d = a) op_tokens in
        let option2 = List.find_opt (fun (c,d) -> d = b) op_tokens in
        match option1 with
        | Some (op1,token1) ->
           begin match option2 with
           | Some (op2, token2) ->
              if op1 <> op2 then
                List.cons (op1,op2) acc
              else acc
           | None -> acc end
        | None -> acc) [] aux

  let no_stab_analysis fun_list op_tokens conf_relation =
    List.fold_left (fun acc (a,b) ->
        let option1 = List.find_opt (fun (c,d) -> d = a) op_tokens in
        let option2 = List.find_opt (fun (c,d) -> d = b) op_tokens in
        match option1 with
        | Some (op1,token1) ->
           begin match option2 with
           | Some (op2, token2) ->
              if op1 = op2 then
                List.cons (op1,op2) acc
              else acc
           | None -> acc end
        | None -> acc) [] conf_relation

  let same_name_pairs fun_list no_analysis =
    List.fold_left (fun acc a ->
        let option1 =
          List.find_opt (fun (c,d) -> c = a.rs_name.id_string) no_analysis in
        match option1 with
        | Some (op,token) ->
           acc
        | None ->
           a :: acc) [] fun_list

  let parsing line operation_tokens conflict_relation arg_tokens dependency_relation all_tokens =
    match line with
    | word1 :: word2 :: rl ->
       if word1 = "token" then
           let aux = ref all_tokens in
           let operation_tokens =
             List.fold_left (fun acc a ->
                 if not (List.mem a !aux) then
                   begin
                   aux := a :: !aux;
                   List.cons (word2, a) acc end
                 else acc) operation_tokens rl in
           let all_tokens = !aux in
           (operation_tokens,conflict_relation,arg_tokens,dependency_relation,all_tokens)
       else if word2 = "conflicts" then
         let conflict_relation = List.cons (word1,(List.hd rl)) conflict_relation in
         (operation_tokens,conflict_relation,arg_tokens,dependency_relation,all_tokens)
       else if word1 = "argtoken" then
         if not(List.mem (List.nth rl 1) all_tokens) then
           let arg_tokens = List.cons (word2, (List.hd rl), (List.nth rl 1)) arg_tokens  in
           let all_tokens = (List.nth rl 1) :: all_tokens in
           (operation_tokens,conflict_relation,arg_tokens,dependency_relation,all_tokens)
         else (operation_tokens,conflict_relation,arg_tokens,dependency_relation,all_tokens)
       else if word2 = "depends" then
         let dependency_relation = List.cons (word1,(List.hd rl)) dependency_relation in
         (operation_tokens,conflict_relation,arg_tokens,dependency_relation,all_tokens)
       else (operation_tokens,conflict_relation,arg_tokens,dependency_relation,all_tokens)
    | _ -> (operation_tokens,conflict_relation,arg_tokens,dependency_relation,all_tokens)

  let input_line_opt ic =
    try Some (input_line ic)
    with End_of_file -> None

  let read_lines ic =
    let op_tokens = ref [] in
    let conf_relation = ref [] in
    let arg_tokens = ref [] in
    let all_tokens = ref [] in
    let dep_relation = ref [] in
    let rec aux op_tokens conf_relation arg_tokens dep_relation all_tokens =
      match input_line_opt ic with
      | Some line ->
         let words = String.split_on_char ' ' line in
         let (operation_tokens,conflict_relation, a_tokens, d_relation, all_t) =
           parsing words !op_tokens !conf_relation !arg_tokens !dep_relation !all_tokens in
         op_tokens := operation_tokens;
         conf_relation := conflict_relation;
         arg_tokens := a_tokens;
         all_tokens := all_t;
         dep_relation := d_relation;
         aux op_tokens conf_relation arg_tokens dep_relation all_tokens;
      | None -> close_in ic
    in
    aux op_tokens conf_relation arg_tokens dep_relation all_tokens;
    (!op_tokens , !conf_relation, !arg_tokens, !dep_relation)

  let arg_tokens_check arg_tokens fun_list =
     List.fold_left (fun acc (a,b,c) ->
        let exists_op =
          List.exists (fun d -> a = d.rs_name.id_string) fun_list in
        if exists_op then
          let exists_arg =
            List.exists (fun d ->
                let arg_list = d.rs_cty.cty_args in
                List.exists (fun a ->
                  (a.pv_vs.vs_name.id_string) = b) arg_list) fun_list in
          if exists_arg then (a,b,c) :: acc
          else acc
        else acc ) [] arg_tokens

  let same_op_arg_tokens conf_relation arg_tokens =
    List.fold_left (fun acc (a,b) ->
        let (op1,arg1,t1) = List.hd (List.filter (fun (c,d,e) ->
                                  a = e) arg_tokens) in
        let (op2,arg2,t2) = List.hd (List.filter (fun (c,d,e) ->
                                         b = e) arg_tokens) in
        let aux = ((op1,arg1,t1),(op2,arg2,t2)) in
        if op1 = op2 then
          aux :: acc
        else acc) [] conf_relation

  let diff_op_arg_tokens conf_relation arg_tokens =
    List.fold_left (fun acc (a,b) ->
        let (op1,arg1,t1) = List.hd (List.filter (fun (c,d,e) ->
                                         a = e) arg_tokens) in
        let (op2,arg2,t2) = List.hd (List.filter (fun (c,d,e) ->
                                         b = e) arg_tokens) in
        let aux = ((op1,arg1,t1),(op2,arg2,t2)) in
        if op1 <> op2 then
          aux :: acc
        else acc) [] conf_relation
    
  let dep_pairs fun_l not_to_analyse =
    List.fold_left (fun acc a ->
        List.fold_left (fun acc1 a1 ->
            if (a1.rs_name.id_string <>
                  a.rs_name.id_string)
               && (not (List.mem (a1.rs_name.id_string
                                 ,a.rs_name.id_string) not_to_analyse)) then
              List.cons (a1,a) acc1
            else acc1) acc fun_l) [] fun_l

  let test_function op1 op2 fun_list =
    let func1 = List.find (fun a -> a.rs_name.id_string = op1) fun_list in
    let func2 = List.find (fun a -> a.rs_name.id_string = op2) fun_list in
    let pre_list1 = List.map (fun a ->
                        term_ptree a.Term.t_node) func1.rs_cty.cty_pre in
    let pre_list2 = List.map (fun a ->
                        term_ptree a.Term.t_node) func2.rs_cty.cty_pre in
    let counter = ref 0 in
    List.iter (fun a -> List.iter (fun b ->
                            let tmp = lexical_relation a.term_desc b.term_desc in
                            if tmp then incr counter ) pre_list2) pre_list1

  let add_comm_module ?(path=[]) env =
    open_file env path;
    let name_list = fun_find () in
    (* let fun1_name = List.hd name_list in
     * let fun2_name = List.nth name_list 1 in  *)
    let fun_l = fun_list () in
    let (op_tokens, arg_tokens, conf_relation, dep_relation) = match opt_tokens with
      | None -> ([], [], [], [])
      | Some s -> let file = open_in s in
          let (op_tokens, conf_relation, arg_tokens, dep_relation) = read_lines file in
          let op_tokens =
            List.filter (fun (a,b) -> List.mem a name_list) op_tokens in
          let token_list =
            List.fold_left (fun acc (a,b) -> List.cons b acc) [] op_tokens in
          let arg_tokens = arg_tokens_check arg_tokens fun_l in
          let op_conf_relation =
            List.filter (fun (a,b) -> List.mem a token_list &&
                                      List.mem b token_list) conf_relation in
          let arg_conf_relation =
            List.filter (fun (a,b) ->
                List.exists (fun (c,d,e) -> a = e) arg_tokens &&
                List.exists (fun (c,d,e) -> b = e) arg_tokens) conf_relation in
          let conf_relation = List.append op_conf_relation arg_conf_relation in
          eprintf "Tamanho igual a %d" (List.length dep_relation);
          let dep_relation = List.filter
                               (fun (a,b) ->
                                 List.mem a token_list &&
                                   List.mem b token_list) dep_relation in
          (op_tokens, arg_tokens, conf_relation, dep_relation) in
    let id = mk_id "CiseAnalysis" in
    open_module id;
    let state_eq = state_eq_fun () in
    let state_fields = get_fields_names (); in
    let state_id = state_ident () in
    add_use_module () ;
    if (state_eq = "") then add_predicate state_fields state_id;
    let not_to_analyse = no_analysis name_list op_tokens conf_relation in
    let dif_fun_name_pairs = diff_name_pairs name_list not_to_analyse in
    let not_to_analyse = no_stab_analysis name_list op_tokens conf_relation in
    let same_fun_name_pairs = same_name_pairs fun_l not_to_analyse in
    let not_to_analyse = no_analysis name_list op_tokens dep_relation in
    List.iter (fun (a,b) -> eprintf "%s  %s" a b) not_to_analyse;
    let dep_analysis_pairs = dep_pairs fun_l not_to_analyse in
    let same_fun_arg_tokens = ref [] in
    let diff_fun_arg_tokens = ref [] in
    if (List.length arg_tokens > 0) && (List.length conf_relation > 0) then
      begin
        same_fun_arg_tokens := same_op_arg_tokens conf_relation arg_tokens;
        diff_fun_arg_tokens := diff_op_arg_tokens conf_relation arg_tokens;
      end;
    List.iter (fun (a,b) ->
        let op_arg_tokens = List.fold_left (fun acc ((op1,arg1,t1),(op2,arg2,t2)) ->
                                if (a = op1 &&  b = op2) then
                                  ((op1,arg1,t1),(op2,arg2,t2)) :: acc
                                else if (a = op2 &&  b = op1) then
                                  ((op2,arg2,t2),(op1,arg1,t1)) :: acc
                                else acc) []  !diff_fun_arg_tokens in
        commutativity fun_l a b state_eq op_arg_tokens) dif_fun_name_pairs;
    List.iter (fun a ->
        let op_arg_tokens = List.filter (fun ((op1,arg1,t1),_) ->
                                a.rs_name.id_string = op1 ) !same_fun_arg_tokens in
        stability a name_list op_arg_tokens state_eq) same_fun_name_pairs;
    List.iter (fun (a,b) ->
        dependency_analysis a.rs_name.id_string
          b.rs_name.id_string fun_l state_eq) dep_analysis_pairs;
    Typing.close_module Loc.dummy_position;
    close_file ()

end

open Ident
open Theory
open Pmodule

let () =
  let mm = Comm.add_comm_module env in
  let fname_gen = (Filename.chop_extension fname) ^ "__cise.mlw" in
  let cout = open_out fname_gen in
  let fmt = Format.formatter_of_out_channel cout in
  let print_m _ m = Format.fprintf fmt "%a@\n@." Pmodule.print_module m in
  let add_m _ m mm = Mid.add m.mod_theory.th_name m mm in
  (* let result = Comm.state_gen () in *)
  Mid.iter print_m (Mstr.fold add_m mm Mid.empty);
  close_out cout

open While_ast
(* open While_parser
 * open While_lexer *)
   
let () =
  let lexbuf = Lexing.from_channel (open_in "test1.sp") in
  let prog = While_parser.prog While_lexer.token lexbuf in
  match prog with
  | None -> Format.eprintf "empty sp file@."
  | Some p -> pp_program p;
              let body = p.contract_e in
              let first = List.hd p.contract_pre in
              let p_list =
                List.filter (fun a ->
                    (a <> first)
                    && (a.expr_desc <> Eskip)) p.contract_pre in
              let pre_list = List.fold_left (fun acc e ->
                                 mk_expr (Eand (acc, e)) Loc.dummy_position)
                                   first p_list in
              let sp = Sp.sp body pre_list in
              Format.eprintf "@\nGenerated sp: %a@." pp_expr sp

let read_channel env path file _c =
  Format.eprintf "hello@.";
  let mlw_file = Env.read_file Pmodule.mlw_language env file in
  let mm_comm = Comm.add_comm_module ~path env in
  Mstr.set_union mm_comm mlw_file
